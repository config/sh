#!/bin/sh

# Initialization file for every interactive posix shell,
# especially useful for dash.
ENV="${HOME}/.shinit"; export ENV

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi
